use chrono::prelude::*;
use models::StationBoard;
use std::error::Error;
use std::sync::{Arc, Mutex};

pub use crate::endpoint::Endpoint;
use crate::endpoint::Product;
pub use crate::models::Journey;
pub use crate::models::Journeys;
pub use crate::models::Leg;
pub use crate::models::Place;
pub use crate::models::StopOver;

mod endpoint;
mod models;
mod request;
mod response;

pub type BoxError = Box<dyn std::error::Error + Send + Sync>;

#[derive(Clone, Debug)]
pub struct Client {
    pub endpoint: Endpoint,
    pub journeys_config: JourneysConfig,
    pub locations_config: LocationsConfig,
    pub stationboard_config: StationBoardConfig,
}

impl Client {
    pub fn new(id: usize) -> Result<Self, Box<dyn Error>> {
        let endpoint = &Endpoint::infos().unwrap()[id];
        Ok(Client {
            endpoint: endpoint.clone(),
            journeys_config: JourneysConfig::default(endpoint),
            locations_config: LocationsConfig::default(),
            stationboard_config: StationBoardConfig::default(endpoint),
        })
    }

    pub async fn get_journeys(&self) -> Result<Journeys, BoxError> {
        self.endpoint.journeys(&self.journeys_config, None).await
    }

    pub async fn get_earlier_journeys(&self) -> Result<Journeys, BoxError> {
        self.endpoint
            .journeys(
                &self.journeys_config,
                Some("earlierRef".to_string()), // TODO: find better solution
            )
            .await
    }

    pub async fn get_later_journeys(&self) -> Result<Journeys, BoxError> {
        self.endpoint
            .journeys(
                &self.journeys_config,
                Some("laterRef".to_string()), // TODO: find better solution
            )
            .await
    }

    pub async fn refresh_journey(&self, journey: &Journey) -> Result<Journey, BoxError> {
        self.endpoint.update_journey(&journey.refreshToken).await
    }

    pub async fn get_locations(&self, location_raw: String) -> Result<Vec<Place>, BoxError> {
        let max_results = *self.locations_config.max_results.lock().unwrap();
        self.endpoint.locations(&location_raw, &max_results).await
    }

    pub async fn get_stationboard(&self) -> Result<Vec<StationBoard>, BoxError> {
        self.endpoint.stationboard(&self.stationboard_config).await
    }

    pub fn set_stationboard_id(&self, location_id: Option<i64>) -> &Self {
        let mut station_id = self.stationboard_config.station_id.lock().unwrap();
        *station_id = location_id;
        self
    }

    pub fn set_departure_id(&self, location_id: Option<i64>) -> &Self {
        let mut departure = self.journeys_config.departure.lock().unwrap();
        *departure = location_id;
        self
    }

    pub fn get_departure_id(&self) -> Option<i64> {
        let departure = self.journeys_config.departure.lock().unwrap();
        *departure
    }

    pub fn set_arrival_id(&self, location_id: Option<i64>) -> &Self {
        let mut arrival = self.journeys_config.arrival.lock().unwrap();
        *arrival = location_id;
        self
    }

    pub fn get_arrival_id(&self) -> Option<i64> {
        let arrival = self.journeys_config.arrival.lock().unwrap();
        *arrival
    }

    // only one stopover supported atm
    pub fn set_journeys_via(&self, location_id: Option<i64>) -> &Self {
        let mut via = self.journeys_config.via.lock().unwrap();
        match location_id {
            Some(l) => *via = vec![l],
            None => *via = Vec::new(),
        }
        self
    }

    // only one stopover supported atm
    pub fn get_journeys_via(&self) -> Option<i64> {
        let via = self.journeys_config.via.lock().unwrap();
        if via.len() == 1 {
            Some(via[0])
        } else {
            None
        }
    }

    pub fn get_datetime_type(&self) -> TimeType {
        let datetime_type = self.journeys_config.datetime_type.lock().unwrap();
        datetime_type.clone()
    }

    pub fn set_datetime_type(&self, datetime: TimeType) -> &Self {
        let mut datetime_type = self.journeys_config.datetime_type.lock().unwrap();
        *datetime_type = datetime;
        self
    }
}

#[derive(Clone, Debug)]
pub struct JourneysConfig {
    pub departure: Arc<Mutex<Option<i64>>>, // id of departure station
    pub arrival: Arc<Mutex<Option<i64>>>,   // id of arrival station
    pub via: Arc<Mutex<Vec<i64>>>,
    pub earlier_ref: Arc<Mutex<Option<String>>>,
    pub later_ref: Arc<Mutex<Option<String>>>,
    pub datetime: Arc<Mutex<Option<DateTime<Local>>>>, // Compute journeys departing/arriving at this date/time.
    pub datetime_type: Arc<Mutex<TimeType>>, // Define if Journey time is departure or arriving time
    pub results: Arc<Mutex<Option<i32>>>,    // Max. number of journeys. - integer - 3
    pub stopovers: Arc<Mutex<bool>>, // Fetch & parse stopovers on the way? - boolean - false
    pub max_transfers: Arc<Mutex<Option<i32>>>, // Maximum number of transfers. - integer - let HAFAS decide
    pub transfer_time: Arc<Mutex<Option<i32>>>, // Minimum time in minutes for a single transfer. - integer - 0
    // pub accessibility: String, // partial or complete. - string - not accessible
    // pub bike: bool, // Compute only bike-friendly journeys? - boolean - false
    // pub start_with_walking: bool, // Consider walking to nearby stations at the beginning of a journey? - boolean - true
    // pub walking_speed: String, // slow, normal or fast. - string - normal
    // pub tickets: bool, // Return information about available tickets? - boolean - false
    // pub polylines: bool, // Fetch & parse a shape for each journey leg? - boolean - false
    // pub remarks: bool, // Parse & return hints & warnings? - boolean - true
    // pub scheduled_days: bool, // Parse & return dates each journey is valid on? - boolean - false
    // pub language: String, // Language of the results. - string - en
    pub products_used: Vec<ProductUsed>,
    // pub national_express: bool, // Include InterCityExpress (ICE)? - boolean - true
    // pub national: bool,        // Include InterCity & EuroCity (IC/EC)? - boolean - true
    // pub regional_exp: bool,     // Include RegionalExpress & InterRegio (RE/IR)? - boolean - true
    // pub regional: bool,        // Include Regio (RB)? - boolean - true
    // pub suburban: bool,        // Include S-Bahn (S)? - boolean - true
    // pub bus: bool,             // Include Bus (B)? - boolean - true
    // pub ferry: bool,           // Include Ferry (F)? - boolean - true
    // pub subway: bool,          // Include U-Bahn (U)? - boolean - true
    // pub tram: bool,            // Include Tram (T)? - boolean - true
    // pub taxi: bool,            // Include Group Taxi (Taxi)? - boolean - true
}

impl JourneysConfig {
    pub fn default(endpoint: &Endpoint) -> Self {
        let mut products_used = Vec::new();
        for p in endpoint.options.products.iter() {
            let product_used = ProductUsed {
                used: Arc::new(Mutex::new(true)),
                product: p.clone(),
            };
            products_used.push(product_used);
        }

        JourneysConfig {
            departure: Arc::new(Mutex::new(None)),
            arrival: Arc::new(Mutex::new(None)),
            via: Arc::new(Mutex::new(Vec::new())),
            earlier_ref: Arc::new(Mutex::new(None)),
            later_ref: Arc::new(Mutex::new(None)),
            datetime: Arc::new(Mutex::new(None)),
            datetime_type: Arc::new(Mutex::new(TimeType::Departure)),
            results: Arc::new(Mutex::new(None)), // Max. number of journeys. - integer - 3
            stopovers: Arc::new(Mutex::new(true)), // Fetch & parse stopovers on the way? - boolean - false
            max_transfers: Arc::new(Mutex::new(None)), // Maximum number of transfers. - integer - let HAFAS decide
            transfer_time: Arc::new(Mutex::new(None)), // Minimum time in minutes for a single transfer. - integer - 0
            // pub accessibility: String, // partial or complete. - string - not accessible
            // pub bike: bool, // Compute only bike-friendly journeys? - boolean - false
            // pub startWithWalking: bool, // Consider walking to nearby stations at the beginning of a journey? - boolean - true
            // pub walkingSpeed: String, // slow, normal or fast. - string - normal
            // pub tickets: bool, // Return information about available tickets? - boolean - false
            // pub polylines: bool, // Fetch & parse a shape for each journey leg? - boolean - false
            // pub remarks: bool, // Parse & return hints & warnings? - boolean - true
            // pub scheduledDays: bool, // Parse & return dates each journey is valid on? - boolean - false
            // pub language: String, // Language of the results. - string - en
            products_used,
        }
    }
}

#[derive(Clone, Debug)]
pub struct StationBoardConfig {
    pub station_id: Arc<Mutex<Option<i64>>>,
    pub datetime: Arc<Mutex<Option<DateTime<Local>>>>,
    pub datetime_type: Arc<Mutex<TimeType>>,
    pub results: Arc<Mutex<Option<i32>>>,
    pub products_used: Vec<ProductUsed>,
}

impl StationBoardConfig {
    pub fn default(endpoint: &Endpoint) -> Self {
        let mut products_used = Vec::new();
        for p in endpoint.options.products.iter() {
            let product_used = ProductUsed {
                used: Arc::new(Mutex::new(true)),
                product: p.clone(),
            };
            products_used.push(product_used);
        }

        StationBoardConfig {
            station_id: Arc::new(Mutex::new(None)),
            datetime: Arc::new(Mutex::new(None)),
            datetime_type: Arc::new(Mutex::new(TimeType::Departure)),
            results: Arc::new(Mutex::new(Some(12))),
            products_used,
        }
    }
}

#[derive(Clone, Debug)]
pub struct LocationsConfig {
    max_results: Arc<Mutex<Option<usize>>>,
}

impl LocationsConfig {
    pub fn default() -> Self {
        LocationsConfig {
            max_results: Arc::new(Mutex::new(Some(5))),
        }
    }
}

impl Journeys {
    pub async fn get_earlier(&self, client: &Client) -> Result<Journeys, BoxError> {
        client
            .endpoint
            .journeys(
                &client.journeys_config,
                Some("earlierRef".to_string()), // TODO: find better solution
            )
            .await
    }
    pub async fn get_later(&self, client: &Client) -> Result<Journeys, BoxError> {
        client
            .endpoint
            .journeys(
                &client.journeys_config,
                Some("laterRef".to_string()), // TODO: find better solution
            )
            .await
    }
}

#[derive(Debug, Clone)]
pub struct ProductUsed {
    pub used: Arc<Mutex<bool>>,
    pub product: Product,
}

#[derive(Debug, Clone)]
pub enum TimeType {
    Departure,
    Arrival,
}
