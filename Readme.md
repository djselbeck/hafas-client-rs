# hafas-client-rs

A rust crate to access the "mobile APIs" of [HAFAS public transport management systems](https://de.wikipedia.org/wiki/HAFAS)
in usable way.

In order to achieve this, hafas-client-rs converts the pretty verboose api of
Hafas to an API close to [this one](https://v5.db.transport.rest/api.html).

The goal is to read all enpoint specific information from a Json file of 
[this](https://github.com/public-transport/transport-apis) structure. 
That way a new enppoint can be added by adding the according Json file
and the Json files already exist for most known endpoints.
 
hafas-client-rs is build to be used by the app [Public Transport](https://gitlab.com/terence97/publictransport)
once it is in a usable state.

Not functional yet.

## Related

- The JavaScrpt equivalent library: <https://github.com/public-transport/hafas-client>
- Json-Data with enpoint specific infoirmation: <https://github.com/public-transport/transport-apis>
- Basic info about hafas mgate api: <https://github.com/public-transport/hafas-client/blob/e9701648eeaf8c4e3a0f53c202d0f3bcd29ff954/docs/hafas-mgate-api.md>

